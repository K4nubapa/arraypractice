﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>
int main()
{
    //Подготовка массива
    const int n = 7;
    const int m = 5;
    int mas[n][m];

    //День календаря
    int n_sum = 0;
    time_t t;
    time(&t);
    int day = localtime(&t) -> tm_mday;

    
    //Циклы для вывода массива в консось
    std::cout << "Array: " << "\n";
    for (int i = 0; i < n; i++) 
    {
        for (int j = 0; j < m; j++) 
        {

            mas[i][j] = i + j;
            std::cout << mas[i][j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
    
    for (int y = 0; y < n; y++) {
        n_sum += mas[day % n][y];
    }
    std::cout <<"Sum of elements: " << n_sum;
}

